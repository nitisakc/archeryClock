#include <EEPROM.h>

//#define constantName value
#define COUNTDOWN_PREPARE 10  //countdown 10 seconds to prepare to walk-in shooting line
#define DISPLAY_LENGTH    3   //display 4 digits from previous set to display 8 digits
#define RESET_TIMEOUT     1
#define BTN_IN            2   //button = digital pin 2
#define BUZZER_OUT        53  //buzzer = digital pin 53

/*
180   //qualification outdoor - 6 arrows x 30 sec
90    //qualification indoor - 3 arrows x 30 sec
30    //shoot-off qualification round - 1 arrow x 30 sec
20    //shoot-off elimination round - 1 arrow x 20 sec
====================================================================
120   //team 3 archers - 2 arrows each x 20 sec
60    //shoot-off team 3 archers - 1 arrow each x 20 sec
80    //mixed team 2 archers - 2 arrows each x 20 sec
40    //shoot-off mixed team 2 archers - 1 arrow each x 20 sec
====================================================================
20    //shoot-off individual 'Alternate shooting' - 1 arrow x 20 sec
====================================================================
900   //break 15 minutes
*/

int segment1Pins[] = {10, 11, 12, 13, 14, 15, 16}; // กำหนดขา Digital สำหรับแต่ละ segment
int segment2Pins[] = {20, 21, 22, 23, 24, 25, 26};
int segment3Pins[] = {30, 31, 32, 33, 34, 35, 36}; //
int digits[10][7] = { // กำหนด pattern ของแต่ละเลข
  {1, 1, 1, 1, 1, 1, 0}, // 0
  {0, 1, 1, 0, 0, 0, 0}, // 1
  {1, 1, 0, 1, 1, 0, 1}, // 2
  {1, 1, 1, 1, 0, 0, 1}, // 3
  {0, 1, 1, 0, 0, 1, 1}, // 4
  {1, 0, 1, 1, 0, 1, 1}, // 5
  {1, 0, 1, 1, 1, 1, 1}, // 6
  {1, 1, 1, 0, 0, 0, 0}, // 7
  {1, 1, 1, 1, 1, 1, 1}, // 8
  {1, 1, 1, 1, 0, 1, 1}  // 9
};

const int _TIME[] = {900, 180, 90, 30, 20, 120, 60, 80, 40};
unsigned long prevSecs = 0, 
              currSecs = 0,
              prevMillis = 0, 
              currMillis = 0,
              prevConfSecs = 0, 
              currConfSecs = 0;
int prevBtn = 1, 
    state = 0, 
    countdown = COUNTDOWN_PREPARE,
    prevSelTime = 0,
    countReset = 0,
    buzzerCount = 0,
    currTime = 0,
    confCount = 0,
    indexTime = 0;
//bool variable name = value to assign to that variable
bool counting = false,
     confMode = false,
     confToggle = false,
     buzzerShootingStart = false;

void setup() {
  Serial.begin(9600);
  pinMode(BTN_IN, INPUT_PULLUP);
  pinMode(BUZZER_OUT, OUTPUT);

  for (int i = 0; i < 7; i++) {
    pinMode(segment1Pins[i], OUTPUT);
    pinMode(segment2Pins[i], OUTPUT);
    pinMode(segment3Pins[i], OUTPUT);
  }

  clearDisplay();
  Serial.println("READY");
  indexTime = EEPROM.read(0);
//  indexTime = 3;
  if(indexTime < 0){ indexTime = 0; }
}

int selTime(){
  return _TIME[indexTime];
}

bool checkDigit(int check){
  if (check == 0 || check == 1 || check == 2 || check == 3 || check == 4 || check == 5 || check == 6 || check == 7 || check == 8 || check == 9){
    return true;
  }else{
    return false;
  }
}

void display(String str){
  Serial.println("display: "+str);
  if(str.length() > DISPLAY_LENGTH){ str = str.substring(0,DISPLAY_LENGTH); } 
  if(str.length() == 0){ str = "   "; }
  else if(str.length() == 1){ str = "  " + str; }
  else  if(str.length() == 2){ str = " " + str; }
  
  int sum = str.toInt();
  int a = str.substring(2,3).toInt();
  int b = str.substring(1,2).toInt();
  int c = str.substring(0,1).toInt();

  if (checkDigit(c) && sum >= 100){
    for (int i = 0; i < 7; i++) { digitalWrite(segment3Pins[i], digits[c][i]); }
  }else{
    for (int i = 0; i < 7; i++) { digitalWrite(segment3Pins[i], 0); }
  }
  
  if (checkDigit(b) && sum >= 10){
    for (int i = 0; i < 7; i++) { digitalWrite(segment2Pins[i], digits[b][i]); }
  }else{
    for (int i = 0; i < 7; i++) { digitalWrite(segment2Pins[i], 0); }
  }
  
  if (checkDigit(a)){
    for (int i = 0; i < 7; i++) { digitalWrite(segment1Pins[i], digits[a][i]); }
  }else{
    for (int i = 0; i < 7; i++) { digitalWrite(segment1Pins[i], 0); }
  }
}

void clearDisplay(){
  for (int i = 0; i < 7; i++) {
    digitalWrite(segment1Pins[i], 0);
    digitalWrite(segment2Pins[i], 0);
    digitalWrite(segment3Pins[i], 0);
  }
}

void reset(){
  countdown = COUNTDOWN_PREPARE;
  counting = false;
  state = 0;
  prevSelTime = 0;
  buzzerCount = 0;
  countReset = 0;
  buzzerShootingStart = false;
  Serial.println("End Loop");
}

void loop() {
  currConfSecs = currMillis = currSecs = millis();    //ใช้ millis คอยให้เช็ค ว่ามีการกดปุ่มหรือไม่
  int currBtn = digitalRead(BTN_IN);
  
  if(counting){
    if(countReset >= RESET_TIMEOUT && currBtn == 1 && prevBtn == 0){      //Reset buzzer ดัง 5 ที
      for (int i = 0; i < 5; i++) {
         digitalWrite(BUZZER_OUT, 1);
         delay(200);
         digitalWrite(BUZZER_OUT, 0);
         delay(200);
      }
      reset();
      delay(1000);
    }else{
      if ((currMillis - prevMillis) > 700) {      //Freq 0.7sec buzzer ดัง 2 ที
        prevMillis = currMillis;
        if(buzzerCount == 0 || buzzerCount == 2 || buzzerShootingStart){
          digitalWrite(BUZZER_OUT, 1);
          buzzerShootingStart = false;
        }else if(state != 1 || !buzzerShootingStart){
          digitalWrite(BUZZER_OUT, 0);
        }
        buzzerCount++;
      }
      
      if ((currSecs - prevSecs) > 1000) {         //Freq 1 sec buzzer ดัง 1 ที
        prevSecs = currSecs;
        
        if(currBtn == 0){ countReset++; }
        else{ countReset = 0; }
  
        if(state == 1){                           //State Shooting Start x-0 
          if(currTime == countdown){
            buzzerShootingStart = true;
          }else{
            buzzerShootingStart = false;
          }
          
          display(String(countdown)); 
          countdown--;
  
          if(countdown < 0){                      //buzzer ดัง 3 ที
             for (int i = 0; i < 3; i++) {
                digitalWrite(BUZZER_OUT, 1);
                delay(700);
                digitalWrite(BUZZER_OUT, 0);
                delay(700);
             }
             reset();
          }
        }
        else if(state == 0){                      //State Countdown 10-1 
          display(String(countdown)); 
          countdown--;
  
          if(countdown == 0){                     //End Countdown
            currTime = countdown = selTime();     //Get time from selecter
            state = 1;
          }
        }
      }
    }
  }else{
    digitalWrite(BUZZER_OUT, 0);

    //Button Press and Release
    if(currBtn == 1 && prevBtn == 0 && !confMode){ 
      counting = true; 
    }

    if(currBtn == 0){
      if ((currConfSecs - prevConfSecs) > 500) {
          prevConfSecs = currConfSecs;
          confCount++;
          if(confCount >= 4){
            confMode = true;
            confCount = 0;
            indexTime++;
            if(indexTime >= (sizeof(_TIME) / sizeof(int))){
              indexTime = 0;
            }
            EEPROM.write(0, indexTime);
          }
      }
    }else{
      confMode = false;
      confCount = 0;  
    }

    if(confMode){
      display(String(selTime()));

      ////-----------------Hardware เป็น Relay ไม่ควรกระพริบเร็วๆ
      // if(confToggle){
      //   //module.clearDisplay();
      //   display("        ");
      // }else{
      //   display(" " + String(selTime()) + " ");
      // }
      // confToggle = !confToggle;
    }else{
      //Update 7-seg
      int currSelTime = selTime();
      if(prevSelTime != currSelTime){ 
        display(String(currSelTime)); 
      }
      prevSelTime = currSelTime;
    }
    
    delay(100);
  }
  prevBtn = currBtn;
}
